#!/bin/sh
echo $LD_LIBRARY_PATH | egrep "/opt/alfresco-5.0.a/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/opt/alfresco-5.0.a/java/bin:/opt/alfresco-5.0.a/common/bin:$PATH"
export PATH
LD_LIBRARY_PATH="/opt/alfresco-5.0.a/common/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH
fi

##### IMAGEMAGICK ENV #####
MAGICK_HOME="/opt/alfresco-5.0.a/common"
export MAGICK_HOME
MAGICK_CONFIGURE_PATH="/opt/alfresco-5.0.a/common/lib/ImageMagick-6.8.6/config-Q16"
export MAGICK_CONFIGURE_PATH
MAGICK_CODER_MODULE_PATH="/opt/alfresco-5.0.a/common/lib/ImageMagick-6.8.6/modules-Q16/coders"
export MAGICK_CODER_MODULE_PATH


GS_LIB="/opt/alfresco-5.0.a/common/share/ghostscript/fonts"
export GS_LIB
##### JAVA ENV #####
JAVA_HOME=/opt/alfresco-5.0.a/java
export JAVA_HOME

##### SSL ENV #####
SSL_CERT_FILE=/opt/alfresco-5.0.a/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE


. /opt/alfresco-5.0.a/scripts/build-setenv.sh
