#!/bin/sh
LDFLAGS="-L/opt/alfresco-5.0.a/common/lib $LDFLAGS"
export LDFLAGS
CFLAGS="-I/opt/alfresco-5.0.a/common/include $CFLAGS"
export CFLAGS
		    
PKG_CONFIG_PATH="/opt/alfresco-5.0.a/common/lib/pkgconfig"
export PKG_CONFIG_PATH
